package com.applicationled;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;

import java.io.DataOutputStream;
import java.net.Socket;


public class MainActivity extends AppCompatActivity {

    Client client;
    Thread clientThread;

    Socket socket;
    DataOutputStream out;

    SeekBar redValueBar;
    SeekBar greenValueBar;
    SeekBar yellowValueBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        client = new Client("10.0.0.248", 40001);
        clientThread = new Thread(client);
        clientThread.start();
        System.out.println("Started Client");

        redValueBar = (SeekBar) findViewById(R.id.seekBarRed);
        redValueBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                client.setRedValue(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        greenValueBar = (SeekBar) findViewById(R.id.seekBarGreen);
        greenValueBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                client.setGreenValue(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        yellowValueBar = (SeekBar) findViewById(R.id.seekBarYellow);
        yellowValueBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                client.setYellowValue(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

//        try{
//            System.out.println("Attempting to connect");
//            socket = new Socket("10.0.0.248", 40001);
//            System.out.println("Connected");
//
//            out = new DataOutputStream(socket.getOutputStream());
//        }
//        catch (UnknownHostException u){
//            System.out.println(u);
//        }
//        catch (IOException i){
//            System.out.println(i);
//        }
    }

    public void on(View view){
        if(client.isConnected()) client.setCommand("on");
        else System.out.println("Client not connected");
//        try {
//            out.writeUTF("on");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public void off(View view){
        if(client.isConnected()) client.setCommand("off");
        else System.out.println("Client not connected");
//        try {
//            out.writeUTF("off");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}
