package com.applicationled;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client implements Runnable {
    private Socket socket = null;
    private DataOutputStream out = null;
    private String address;
    private int port;
    private boolean connected;
    private String command;
    private String value;
    boolean updateRed;
    private String redValue;
    boolean updateGreen;
    private String greenValue;
    boolean updateYellow;
    private String yellowValue;

    public Client(String address, int port){
        this.address = address;
        this.port = port;
        connected = false;
        command = "";
        value = "";
        updateRed = true;
        redValue = "0";
        updateGreen = true;
        greenValue = "0";
        updateYellow = true;
        yellowValue = "0";
    }

    @Override
    public void run() {
        try{
            socket = new Socket(address, port);
            connected = true;
            System.out.println("Connected");

            out = new DataOutputStream(socket.getOutputStream());
        }
        catch(UnknownHostException u){
            System.out.println(u);
        }
        catch(IOException i){
            System.out.println(i);
        }

        while(connected){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            if(command.equals("on")) this.on();
//            if(command.equals("off")) this.off();
            if(updateRed || updateGreen || updateYellow) sendCommand();
        }
    }

    public boolean isConnected(){
        return connected;
    }

    public void setCommand(String command){
        this.command = command;
    }

    public void setRedValue(String value){
        this.redValue = value;
        updateRed = true;
    }

    public void setGreenValue(String value) {
        this.greenValue = value;
        updateGreen = true;
    }

    public void setYellowValue(String value) {
        this.yellowValue = value;
        updateYellow = true;
    }

    private void on(){
        command = "";
        try{
            out.writeUTF("on");
        }
        catch(IOException i){
            System.out.println(i);
        }
    }

    private void off(){
        command = "";
        try{
            out.writeUTF("off");
        }
        catch(IOException i){
            System.out.println(i);
        }
    }

    private void value(){
        try{
            out.writeUTF(value);
        }
        catch(IOException i){
            System.out.println(i);
        }
    }

    private void sendCommand(){
        updateRed = false;
        updateGreen = false;
        updateYellow = false;
        try{
            out.writeUTF(redValue + " " + greenValue + " " + yellowValue);
        }
        catch(IOException i){
            System.out.println(i);
        }
    }
}
